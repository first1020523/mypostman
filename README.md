# MyPostman
In this project, I aimed to encompass both REST and SOAP APIs along with their corresponding methodologies. Utilizing Postman, I automated API executions and validations. Additionally, I employed static data-driven testing through .CSV and JSON files. To generate user-friendly reports and facilitate integration with CI & CD tools, I utilized Newman's command-line functionality in conjunction with HTML and HTML Extra report features. 


## Getting started
Anyone who wants to use this frameworks can clone and implement their respective scenarios. 
Softwares needed :
1) For development: Postman (latest version preferred. Download link: https://www.postman.com/downloads/).
2) For execution: Node.JS, Newman. (Download link: https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/)
3) Newman commands used:
<details><summary>To install report types (click to expand)</summary>

- npm install -g newman-reporter-html

- npm install -g newman-reporter-htmlextra

</details>

<details><summary>To generate the reports (click to expand)</summary>


- HTML:
newman run {location with name of the json file of postman collection} -e {location with name of the json file of postman environment} --reporters html --reporter-html-export {location where report will be generated with name of the html file}


- HTML Extra:
newman run {location with name of the json file of postman collection} -e {location with name of the json file of postman environment} --reporters htmlextra --reporter-htmlextra-export {location where report will be generated with name of the html file}

</details>


## Git commands to clone and pull

- Git clone (https://gitlab.com/first1020523/mypostman.git) 
- Git pull origin (MyPostman)

## I have used Postman version v11.1.0

## Project status
In progress: I will be adding more APIs examples.
